-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 16 Des 2019 pada 07.24
-- Versi Server: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_pend`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `daftar`
--

CREATE TABLE `daftar` (
  `nisn` int(11) NOT NULL,
  `nm_lkp` varchar(50) NOT NULL,
  `nama` varchar(15) NOT NULL,
  `jenkel` varchar(15) NOT NULL,
  `tempat_lahir` varchar(50) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `agama` varchar(20) NOT NULL,
  `anake` int(5) NOT NULL,
  `status_anak` varchar(20) NOT NULL,
  `alamat_anak` text NOT NULL,
  `hp` int(20) NOT NULL,
  `nm_sekolah` text NOT NULL,
  `alamat_sklh` text NOT NULL,
  `tahun` int(5) NOT NULL,
  `nomor` int(20) NOT NULL,
  `nmayah` varchar(50) NOT NULL,
  `alamat_ayah` text NOT NULL,
  `pekerjaan_ayah` varchar(50) NOT NULL,
  `nmibu` varchar(50) NOT NULL,
  `alamat_ibu` text NOT NULL,
  `pekerjaan_ibu` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `daftar`
--

INSERT INTO `daftar` (`nisn`, `nm_lkp`, `nama`, `jenkel`, `tempat_lahir`, `tgl_lahir`, `agama`, `anake`, `status_anak`, `alamat_anak`, `hp`, `nm_sekolah`, `alamat_sklh`, `tahun`, `nomor`, `nmayah`, `alamat_ayah`, `pekerjaan_ayah`, `nmibu`, `alamat_ibu`, `pekerjaan_ibu`) VALUES
(1, 'Wiwik Sugiyarti', 'wiwik', 'Perempuan', 'Palembang', '2018-02-08', 'Islam', 4, '', 'silabranti', 2147483647, 'MTs Raudhatus Sholihin', 'Penantian BAnding Agung', 2019, 2147483647, 'Tugiman', 'Pelawi', 'Lainnya', 'Rusmawati', 'pelawi', 'IRT');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jurusan`
--

CREATE TABLE `jurusan` (
  `idjur` int(10) NOT NULL,
  `nmjur` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jurusan`
--

INSERT INTO `jurusan` (`idjur`, `nmjur`) VALUES
(1, 'Akuntansi'),
(2, 'Pemasaran'),
(3, 'Teknik Komputer Jaringan'),
(4, 'Multimedia');

-- --------------------------------------------------------

--
-- Struktur dari tabel `login`
--

CREATE TABLE `login` (
  `username` varchar(20) NOT NULL,
  `password` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `login`
--

INSERT INTO `login` (`username`, `password`) VALUES
('admin', 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengumuman`
--

CREATE TABLE `pengumuman` (
  `idpengumuman` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `jenkel` varchar(14) NOT NULL,
  `statusdaftar` varchar(30) NOT NULL,
  `jadwal` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengumuman`
--

INSERT INTO `pengumuman` (`idpengumuman`, `nama`, `jenkel`, `statusdaftar`, `jadwal`) VALUES
(1, 'dian', 'perempuan', 'terima', 'Selasa, 11-10-2019');

-- --------------------------------------------------------

--
-- Struktur dari tabel `smp`
--

CREATE TABLE `smp` (
  `kdsmp` varchar(12) NOT NULL,
  `namasmp` varchar(50) NOT NULL,
  `alamat_smp` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `uploadijz`
--

CREATE TABLE `uploadijz` (
  `idijz` int(11) NOT NULL,
  `ijazah` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `uploadskhu`
--

CREATE TABLE `uploadskhu` (
  `idskhu` int(11) NOT NULL,
  `skhu` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `daftar`
--
ALTER TABLE `daftar`
  ADD PRIMARY KEY (`nisn`);

--
-- Indexes for table `jurusan`
--
ALTER TABLE `jurusan`
  ADD PRIMARY KEY (`idjur`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pengumuman`
--
ALTER TABLE `pengumuman`
  ADD PRIMARY KEY (`idpengumuman`);

--
-- Indexes for table `smp`
--
ALTER TABLE `smp`
  ADD PRIMARY KEY (`kdsmp`);

--
-- Indexes for table `uploadijz`
--
ALTER TABLE `uploadijz`
  ADD PRIMARY KEY (`idijz`);

--
-- Indexes for table `uploadskhu`
--
ALTER TABLE `uploadskhu`
  ADD PRIMARY KEY (`idskhu`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `daftar`
--
ALTER TABLE `daftar`
  MODIFY `nisn` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `jurusan`
--
ALTER TABLE `jurusan`
  MODIFY `idjur` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `pengumuman`
--
ALTER TABLE `pengumuman`
  MODIFY `idpengumuman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `uploadijz`
--
ALTER TABLE `uploadijz`
  MODIFY `idijz` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `uploadskhu`
--
ALTER TABLE `uploadskhu`
  MODIFY `idskhu` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
